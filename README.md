# DataScienceChallenge

This directory contains a Jupyter Notebook that reads ANAC 2017 flights files inclusive of glossaries and join the information of the glossaries with flights.

The final merged file is included in the `data` folder as a compressed CSV.

All ANAC files obtained from http://www.anac.gov.br/assuntos/dados-e-estatisticas/historico-de-voos
